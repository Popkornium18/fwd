# fwd

FWD accepts messages via a REST interface and forwards them to a messaging service.
Currently only Signal is supported.

## Build Docker image

```
docker build -f docker/Dockerfile .
```

## Setup

Make sure that you configured poetry to create the virtualenv in the project directory.

```
poetry config virtualenvs.in-project true
```

Then create the environment and install the project.
```
poetry shell
poetry install
```

After that, make sure to install the pre-commit hooks.

```
pre-commit install --install-hooks
```
