#!/bin/sh

if [ -z "${FWD_GUNICORN_WORKERS}" ]; then
  echo >&2 "FWD_GUNICORN_WORKERS not set. Defaulting to 4."
  FWD_GUNICORN_WORKERS=4
fi

gunicorn fwd.main:app --workers "${FWD_GUNICORN_WORKERS}" --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000
