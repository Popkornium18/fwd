from __future__ import annotations

import logging
import os
from pathlib import Path

from fastapi import FastAPI
from fastapi import HTTPException
from platformdirs import PlatformDirs

from fwd.registered_tokens import RegisteredTokens
from fwd.registered_tokens import TokenNotFoundError
from fwd.util import get_log_level
from fwd.util import RelayTask

logger = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=get_log_level())
app = FastAPI()


@app.post("/relay")
@app.post("/relay/", include_in_schema=False)
async def relay(task: RelayTask):
    try:
        await RegisteredTokens.post(token=task.token, text=task.message)
    except TokenNotFoundError as e:
        logger.info(f"Unknown token {task.token}")
        raise HTTPException(status_code=404, detail=str(e))


token_config = (
    Path(file) if (file := os.getenv("FWD_CONFIG")) else PlatformDirs("FWD").user_config_path / "config.toml"
)
RegisteredTokens.parse_registered_tokens(token_config)
