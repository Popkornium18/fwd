from __future__ import annotations

import asyncio
import logging
import tomllib
from pathlib import Path
from typing import Any

from fwd.signal import create_signal_relay
from fwd.util import Message
from fwd.util import Relay

logger = logging.getLogger(__name__)


class TokenNotFoundError(Exception):
    def __init__(self, token: str) -> None:
        self.token = token
        super().__init__(f"No such token {token!r}.")


class RegisteredTokens:
    _registered_tokens: dict[str, tuple[Relay, ...]] = dict()

    @classmethod
    async def post(cls, token: str, text: Message) -> bool:
        try:
            results = await asyncio.gather(
                *[poster(text) for poster in RegisteredTokens._registered_tokens[token]]
            )
            return all([results])
        except KeyError:
            raise TokenNotFoundError(token)

    @classmethod
    def parse_registered_tokens(cls, file: Path) -> None:
        try:
            with open(file, mode="rb") as f:
                data = tomllib.load(f)
        except FileNotFoundError as e:
            raise e

        alias_lookup: dict[str, str] = data["alias"]
        tokens: dict[str, Any] = data["token"]
        for token, token_data in tokens.items():
            name: str = token_data["name"]
            relays: list[dict[str, str]] = token_data["relays"]
            relay_funcs: list[Relay] = []
            for relay_data in relays:
                msg_type: str = relay_data["type"]
                alias: str = relay_data["alias"]
                receiver: str = alias_lookup[alias]

                if msg_type == "signal":
                    relay_funcs.append(create_signal_relay(receiver=receiver, token_name=name))
                else:
                    raise Exception(f"Unsupported message type {msg_type!r}")

            if not relays:
                raise Exception(f"No relays configured for token {token!r}")

            cls._registered_tokens[token] = tuple(relay_funcs)

        logger.info(f"Successfully registered {len(cls._registered_tokens)} token(s)")
