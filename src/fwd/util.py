from __future__ import annotations

import logging
import os
from typing import Protocol

from pydantic import BaseModel


class Relay(Protocol):
    async def __call__(self, message: Message) -> bool:
        ...


class Message(BaseModel):
    title: str
    text: str


class RelayTask(BaseModel):
    token: str
    message: Message


def get_log_level() -> int:
    default_level = logging.INFO
    if not (level_name := os.getenv("FWD_LOGLEVEL")):
        # No log level specified
        return default_level
    else:
        parsed_level: int | str = logging.getLevelName(level_name)
        if isinstance(parsed_level, str):
            # getLevelName returns a string if the level was unknown
            return default_level

        return parsed_level
