from __future__ import annotations

import asyncio
import functools
import logging
import os

from signalbot import SignalAPI  # pyright: ignore[reportMissingTypeStubs]

from fwd.util import Message
from fwd.util import Relay

logger = logging.getLogger(__name__)

try:
    signal_api_socket = os.environ["FWD_SIGNAL_API_SOCKET"]
    signal_phone_number = os.environ["FWD_SIGNAL_PHONE_NUMBER"]
except KeyError as e:
    logger.error(f"Missing environment variables: {e}")
    raise

signal = SignalAPI(signal_service=signal_api_socket, phone_number=signal_phone_number)


async def send_signal_message(message: Message, signal: SignalAPI, receiver: str, token_name: str) -> bool:
    text = f"{token_name}: {message.title}\n\n{message.text}"
    coro = signal.send(message=text, receiver=receiver)  # pyright: ignore[reportUnknownMemberType]
    asyncio.create_task(coro)
    logger.info(f"Sending message via Signal to {receiver!r} (Token: {token_name!r})")
    return True


def create_signal_relay(receiver: str, token_name: str) -> Relay:
    return functools.partial(send_signal_message, signal=signal, receiver=receiver, token_name=token_name)
